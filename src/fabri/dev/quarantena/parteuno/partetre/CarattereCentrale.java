package fabri.dev.quarantena.parteuno.partetre;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*•Progettare un metodo che estragga il carattere centrale da una stringa fornita in input 
 * •Se la stringa ha un numero pari di caratteri, estrarre i due caratteri centrali(es. da “magico” deve estrarre “gi”)*/
public class CarattereCentrale {
	
	public static String estraiCarattere(String s) {
		int posizione;
		String c;
		if(s.length() % 2 == 1) {
			posizione = s.length()/2;
			c = s.substring(posizione , posizione+1);
		}else {
			posizione = s.length()/2-1;
			c = s.substring(posizione , posizione+2);
		}
		return c;
	}
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Inserire stringa");
		String enter = br.readLine();
		System.out.println(estraiCarattere(enter));
		

	}

}
