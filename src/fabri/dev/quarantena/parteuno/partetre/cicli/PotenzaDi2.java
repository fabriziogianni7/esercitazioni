package fabri.dev.quarantena.parteuno.partetre.cicli;
/* calcolare le potenze di 2 fino a 2N
*/
public class PotenzaDi2 {

	public static void main(String[] args) {
		//attenzione, essendo un int, se N = 31 java lo converte in numero negativo - dovrebbe essere long
      final int N= Integer.parseInt(args[0]);
       
       int value = 1;
       int i = 0;
       
       while(i<= N) {
    	   System.out.println(value);
    	   value = value*2;
    	   i++;
       }
       
       
    }}