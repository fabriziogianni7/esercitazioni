package fabri.dev.quarantena.parteuno.partetre.cicli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Scrivere un metodo che riceve una stringa e stampa a video il conteggio delle vocali in essa contenute */
public class ContaVocali {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("inserire una stringa: ");
		String s= br.readLine();
		int a=0;
		int e=0;
		int i=0;
		int o=0;
		int u=0;
		
		for(int x = 0; x < s.length(); x++) {
			String l = s.substring(x, x+1);
			switch (l) {
			case "a" : a++;break;
			case "e" : e++;break;
			case "i" : i++;break;
			case "o" : o++;break;
			case "u" : u++;break;
			default: break;
			}
		}
		System.out.print("a: "+a+" e: "+e+" i: "+i+" o: "+o+" u: "+u);
		

	}

}
