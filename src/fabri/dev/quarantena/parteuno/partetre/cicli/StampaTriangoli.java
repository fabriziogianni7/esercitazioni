package fabri.dev.quarantena.parteuno.partetre.cicli;
/*  Scrivere un metodo che, dato un intero positivo dispari N, 
 * stampi un triangolo isoscelela cui base è costituita da N caratteri */
public class StampaTriangoli {

	
	
	/*  First, we see that we need to print 4 spaces for the first row and, as we get down the triangle,
	 *  we need 3 spaces, 2 spaces, 1 space, and no spaces at all for the last row.
	 *  
	 *   Generalizing, we need to print N – r spaces for each row.
	 *   Second, we need an odd number of stars: 1, 3, 5... 
	 *   So, we need to print r x 2 – 1 stars for each row*/
	public static String stampa() {
		int N=5;
		 StringBuilder result = new StringBuilder();
		    for (int r = 1; r <= N; r++) {
		        for (int sp = 1; sp <= N - r; sp++) {
		            result.append(" ");
		        }
		        for (int c = 1; c <= (r * 2) - 1; c++) {
		            result.append("*");
		        }
		        result.append(System.lineSeparator());
		    }
		    return result.toString();
	}
	
	public static void main(String[] args) {
		stampa();
	}
}
