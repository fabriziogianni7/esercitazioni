package fabri.dev.quarantena.parteuno.partetre.cicli;
/*Progettare una classe per la conversione di base dei numeri interi 
 * •Ogni oggetto della classe viene costruito con un intero o con una stringa che contiene l’intero 
 * •La classe è in grado di convertire l’intero nella base desiderata(restituito sotto forma di stringa)
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConvertiBase {

	//1010 (base 2) =  1*2°3+0*2°2 ecc
	public static double convertiInB10(String num, int base) {
		double somma =0;
		for(int i=0; i <= num.length()-1; i++) {
			double n = Double.parseDouble(num.substring(i, i+1));
			double k = n*(Math.pow(base, num.length()-i-1));
			somma += k;
		}
		return somma;
	}
	
	//428 (base 10)->(base 6) = 1552
	public static double convertiDaB10(String num, int base) {
		int n = Integer.parseInt(num);
		int numero = n;
		int somma =0;
		for(int i =0 ; numero>=1; i++) {
			
			int q = numero/base;
			int	 r = (int) (numero%base * Math.pow(10, i));
			numero =q;
			somma +=r;
		}
		return somma;
		
	}
	
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Che numero si vuole convertire?");
		String num = br.readLine();
		System.out.print("Da che base si vuole convertire?");
		int baseIn = Integer.parseInt(br.readLine());
			
		System.out.print("In che base si vuole convertire?");
		int base = Integer.parseInt(br.readLine());
		double numeroConvertito;
		if(base == 10)
			numeroConvertito = ConvertiBase.convertiInB10(num,baseIn);
		else
			numeroConvertito = ConvertiBase.convertiDaB10(num,base);
	
		System.out.print("il numero convertito da base "+baseIn+" in base "+base+" è: "+numeroConvertito);

	}

}
