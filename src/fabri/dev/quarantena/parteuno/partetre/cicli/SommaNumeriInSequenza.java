package fabri.dev.quarantena.parteuno.partetre.cicli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Scrivere un metodo che, dati in ingresso due interi a e b e un terzo intero N, stampi a e b e gli N numeri della 
 * sequenza in cui ogni numero è la somma dei due precedenti 
 * */
public class SommaNumeriInSequenza {

	public static void main(String[] args) throws NumberFormatException, IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Inserire il primo numero: ");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Inserire il secondo numero: ");
		int b = Integer.parseInt(br.readLine());
		System.out.println("Quanti numeri deve contenere la sequenza? ");
		int N = Integer.parseInt(br.readLine());
		System.out.println(a);
		System.out.println(b);
		
		int somma = 0;
		for (int i=0; i<=N; i++) {
			somma = a+b;
			a=b;
			b = somma;
			System.out.println(somma);
			
		}
		
	}
}
