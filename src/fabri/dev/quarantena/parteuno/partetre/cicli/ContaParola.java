package fabri.dev.quarantena.parteuno.partetre.cicli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/*Scrivere un metodo che, presi in ingresso un testo sotto forma di stringa 
 * e una parola w, 
 * trasformi il testo in parole(token) e conti le occorrenze di w nel test
 * */
public class ContaParola {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("inserisci testo: ");
		String testo = br.readLine();
		System.out.println("inserisci parola da contare: ");
		String parola = br.readLine();
		
		String[] tokens = testo.split(" ");
		
		//lambda expression -- avanzato
		List<String> tks = Arrays.asList(tokens);
		long cont = tks.parallelStream().filter(t -> t.equals(parola)).count();
		
//		questo è quello che fa la lambda expression
//		for (int i = 0; i < tokens.length; i++) {
//			if(tokens[i].equals(parola)) 
//				cont++;
//		}
		
		if (cont != 0) 
			System.out.println("le parole corrispondenti a "+parola+" sono "+cont);
		
		System.out.println("non ci sono parole corrispondenti!!");

	}

}
