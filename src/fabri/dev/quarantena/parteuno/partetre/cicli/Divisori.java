package fabri.dev.quarantena.parteuno.partetre.cicli;
/*Scrivere un metodo che, dato un intero positivo n in ingresso, stampi i divisori propri di n (ovvero i divisori < n)*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Divisori {

	public static void main(String[] args) throws NumberFormatException, IOException {
		System.out.print("Inserire numero");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		for(int i=1; i<=n; i++) {
			if(n%i ==0)
				System.out.println(i);
		}

	}

}
