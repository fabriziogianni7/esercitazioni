package fabri.dev.quarantena.parteuno.partetre.cicli.palindromo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PalindromoTester {

	public static void main(String[] args) throws IOException {
		Palindromo pal = new Palindromo();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Inserire parola: ");
		String s = br.readLine();
		if(pal.isPalindromo(s) == true)
			System.out.print("è un palindromo!");
		else
			System.out.print("non è palindromo");
 
			

	}

}
