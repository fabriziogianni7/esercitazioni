package fabri.dev.quarantena.parteuno.partetre.cicli.palindromo;
/*Una stringa è palindromase rimane uguale quando viene letta da sinistra verso destra o da 
 * destra verso sinistra 
 * •Scrivere un metodo che dica che una stringa è palindroma 
 * •Scrivere anche una classe di collaudo che testi il metodo in diverse situazioni*/
public class Palindromo {

	public boolean isPalindromo(String s) {
		
		boolean isPal = true;
		
		String s1 = s.substring(0, s.length()/2);
		String s2 = s.substring(s.length()/2, s.length());
		if(s.length() % 2 == 1) 
			s2 = s.substring(s.length()/2+1, s.length());
			
		int l = s2.length();
		
		for(int i = 0; i <l && isPal==true;i++) {
			char c1 = s1.charAt(i);
			int index2 = l-1-i;
			char c2 = s2.charAt(index2);
			isPal = c1 == c2 ? true : false;
		}
		return isPal;
	}
		
	
}
