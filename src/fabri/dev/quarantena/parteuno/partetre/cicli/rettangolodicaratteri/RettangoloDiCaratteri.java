package fabri.dev.quarantena.parteuno.partetre.cicli.rettangolodicaratteri;
/*Progettare una classe RettangoloDiCaratteri che rappresenta un rettangolo
 *  riempito con caratteri * 
 *  •Un oggetto della classe viene costruito fornendo la posizione x, y e la lunghezza e altezza del rettangolo 
 *  •Il metodo drawsi occupa di stampare il rettangolo a video, partendo dalla posizione (0,0)
*/
public class RettangoloDiCaratteri {
	
	int x;
	int y;
	int altezza;
	int lunghezza;
	
	RettangoloDiCaratteri(int x, int y, int l, int a){
		this.x=x;
		this.y=y;
		this.lunghezza=l;
		this.altezza=a;
	}
	
	public StringBuilder draw() {
		
		StringBuilder result = new StringBuilder();
		
		for(int x=0; x<=altezza; x++) {
			for(int y = 0; y<=lunghezza+this.y; y++) {
				if(y<this.y)
					result.append("x");
				else
					result.append("*");
			}
			System.out.println();
			
		}
		return result;
	}
	public static void main(String[] args) {
		RettangoloDiCaratteri r = new RettangoloDiCaratteri(4, 4, 5, 6);
		System.out.print(r.draw());

	}
	

}
