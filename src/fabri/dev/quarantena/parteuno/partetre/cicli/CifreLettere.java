package fabri.dev.quarantena.parteuno.partetre.cicli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*  
 * �Scrivere un metodo che prenda in ingresso una stringa contenente cifre e restituisca una stringa in
 *  cui ciascuna cifra � stata trasformata nella parola corrispondente 
 *  Ad esempio, data in input la stringa �8452�, il metodo restituisce �otto quattro cinque due� 
 *  
 *  �Viceversa, scrivere un metodo che prenda in ingresso una stringa 
 *  contenente cifre scritte a lettere  e restituisca una stringa contenente le cifre corrispondenti 
 * 	Ad esempio, data in input la stringa �otto quattro cinque due�, il metodo restituisce �8452�
 * 
 * Ad esempio, �8452� viene trasformato in �ottomila quattrocento cinquanta due
 * */
public class CifreLettere {
	private static String[] num = {"1","2","3","4","5","6","7","8","9"};
	private  static String[] lett = {"uno","due","tre","quattro","cinque","sei","sette","otto","nove"};
	private  static String[] prefissiDicia = {"undici","dodici","tredici","quattordici","quindici","sedici","diciasette","diciotto","diciannove"};
	private  static String[] prefissiDecine = {"dieci","venti","trenta","quaranta","cinquanta","sessanta","settanta","ottanta","novanta"};
	
	private static String dieci = "dieci";	
	private static String cento = "cento";	
	private static String mille = "mille";	
	private static String mila = "mila";	
	
	
	public static void restituisciNumero(String lettere) {
		
		String[] lettInput = lettere.split(" ");
		
		for(int i=0; i<=lettInput.length-1; i++) {
			for(int x=0; x<=lett.length-1; x++) {
				if(lettInput[i].equals(lett[x])) {
					System.out.print(num[x]);
				}
			}
		}
		
		
	}
	
	
	
	public static void restituisciStringa(String numero) {
		for(int x= 0; x<= numero.length()-1; x++) {
			for(int i= 0; i<= num.length-1; i++) {
					if(Integer.parseInt(num[i]) == Integer.parseInt(numero.substring(x,x+1))) {
						System.out.print(lett[i]+" ");
					}
			}
		}
		
		
		
	}
	
	
	
	
	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		System.out.println("inserire numeri a lettere: ");
//		String lettere = br.readLine();
//		restituisciNumero(lettere);
//		
//		System.out.println();
//		System.out.println("inserire numeri: ");
//		String numero = br.readLine();


	}

}
