package fabri.dev.quarantena.parteuno.partetre.cicli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Scrivere un metodo che, dato un intero N, stampi una matrice 
 * NxN il cui elemento (i, j) vale: 
 * – 1 se i è un divisore di j (o viceversa); 
 * – 0 altrimenti.
*/

public class ForAnnidati {

	public static void main(String[] args) throws NumberFormatException, IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("inserisci un numero: ");
		final int N = Integer.parseInt(br.readLine());
		
		
		for(int i = 1; i <= N; i++) {
			for(int j = 1; j <= N; j++) {
				if (i/j == 1 || j/i == 1)
					System.out.print(1);
				else
					System.out.print(0);
			}
			System.out.println();
		}

	}

}
