package fabri.dev.quarantena.parteuno.partetre.cicli;
/*Una terna pitagorica � una tripla di numeri interi a, b, c 
 * tali che 1 <= a <= b <= c e a2+b2= c2 
 * 
 * �Ovvero a e b sono i lati di un triangolo rettangolo e c l�ipotenusa 
 * 
 * �Scrivere un metodo che legge un intero N e stampa tutte le triple pitagoriche con c <= N 
 * �Ad esempio: dato N=15 il metodo stampa:

	a=3 b=4 c=5
	a=6 b=8 c=10
	a=5 b=12 c=13
	a=9 b=12 c=15
*/
public class TernaPitagorica {

	public static void getTernaPitagorica(int n) {
		
		for(int c = 0; c<=n; c++) {
			for(int a = 0; a<=n; a++) {
				for(int b =0; b<=n; b++) {
//					1 <= a <= b <= c e a2+b2= c2
					if(1 <= a && a <= b && b <= c && (a*a+b*b == c*c)) {
						System.out.print("a="+a);
						System.out.print(" b="+b);
						System.out.print(" c="+c);
						System.out.println();
						
					}
				}
			}
		}
	}
	
	public static void main(String[] args) {
		getTernaPitagorica(15);

	}

}
