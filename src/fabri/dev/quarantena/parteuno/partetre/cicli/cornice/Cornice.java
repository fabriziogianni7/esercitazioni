package fabri.dev.quarantena.parteuno.partetre.cicli.cornice;
/*Scrivere un metodo che, dato un intero N e una stringa in ingresso, 
 * stampi una cornice NxN all�interno della quale venga visualizzata la stringa
 * (eventualmente suddivisa per righe) */
class Cornice {
	private static int n;
	private static String s;
	
	public Cornice(int n, String s) {
		this.n=n;
		this.s =s;
	}
	
	public void creaCornice() {
		int t = 0;
		for(int x = 0; x <= n; x++) {
			for(int i = 0; i <= n; i++) {
					if(x>0 && x<n && i>0 && i<n ) {
							if(t<s.length()) {
								System.out.print(s.charAt(t));
								t++;
							}else {System.out.print(" ");}
						}else {
							System.out.print("*");
				}
			}
			System.out.println();
				
			}
	}
		

}
