package fabri.dev.quarantena.parteuno.partetre.cicli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Scrivere un metodo che riceve tre stringhe e le stampa in verticale una accanto all’altra */
public class StringaVerticale {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Inserire tre parole: ");
		String s = br.readLine();
		String s2 = br.readLine();
		String s3 = br.readLine();
		int lunghezzaMax = Math.max(s.length(), Math.max(s2.length(),s3.length()));
		
		for(int i = 0; i<lunghezzaMax; i++) {
			if(i >= s.length())
				System.out.print(" ");
			else 
				System.out.print(s.charAt(i));
			if(i >= s2.length())
				System.out.print(" ");
			else 
				System.out.print(s2.charAt(i));
			if(i >= s3.length())
				System.out.print(" ");
			else 
				System.out.print(s3.charAt(i));
			System.out.println();
		}
	}
}