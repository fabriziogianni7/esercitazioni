package fabri.dev.quarantena.parteuno.partetre;
import java.util.Random;
/*Progettare un metodo che emetta sullo standard output un saluto scelto
 *  casualmentetra “ciao”, “hello”, “bella”, “salve” e “morituri te salutant” 
 *  (rendere quest’ultimo doppiamente più probabile)
*/
public class SalutoCasuale {

	private static String saluto;
	
	public String diCiao() {
		
		switch(new Random().nextInt(10)) {
			case 0: saluto = "ciao";
				break;
			case 1: saluto = "suca";
				break;
			case 2: saluto = "come va?";
				break;
			case 3: saluto = "Lozzio!";
				break;
			case 4: saluto = "bonjour";
				break;
			//default fa quell'azione quando non è nessun altro caso, se vogliamo 
			//dargli il doppio delle possibilita che avvenga, bisogna mettere, su 
			//10 casi, solo 5.
			default: saluto = "morituri te salutant";
				break;
		}
		
		return saluto;
		
	}
	public static void main(String[] args) {
		
		System.out.println(new SalutoCasuale().diCiao());

	}

}
