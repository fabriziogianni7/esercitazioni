package fabri.dev.quarantena.parteuno.due.registratorecassa;
/*
 * 
 * Progettare una classe che costituisca un modello di registratore di cassa 
 * �La classe deve consentire a un cassiere di digitarei prezzidi articoli e la quantit� di denaro 
 * pagata dal cliente, calcolando il resto dovuto 
 * 
 * �Quali metodi vi servono? 
 * �Registra il prezzo di vendita per un articolo -> PREZZO
 * �Registra la somma di denaro pagata -> PAGATO
 * �Calcola il resto dovuto al cliente -> PAGATO - PREZZO
 * 
 * 
 * */
public class RegistratoreCassa {
	
	private long prezzo;
	private long denaroPagato;
	
	public RegistratoreCassa(long prezzo, long denaroPagato) {
		this.prezzo = prezzo;
		this.denaroPagato = denaroPagato;
	}
	
	public long calcolaResto() {
		long resto = denaroPagato - prezzo;
		return resto;
	}
	
	//i seguenti metodi ritornano i valori di unput che "riempiono" le variabili di classe
	public long registraDenaroPagato() {
		return denaroPagato;
	}
	public long registraPrezzoVendita() {
		return prezzo;
	}

}
