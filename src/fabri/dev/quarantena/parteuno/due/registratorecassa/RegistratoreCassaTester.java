package fabri.dev.quarantena.parteuno.due.registratorecassa;

public class RegistratoreCassaTester {

	public static void main(String[] args) {
		RegistratoreCassa regi = new RegistratoreCassa(10, 15);
		System.out.println("l'articolo costa: "+regi.registraPrezzoVendita());
		System.out.println("il cliente paga: "+regi.registraDenaroPagato());
		System.out.println("il resto �: "+regi.calcolaResto());
	}

}
