package fabri.dev.quarantena.parteuno.due.puntiesegmenti;
/*
 * Progettare una classe Puntoper la rappresentazione di un punto nello spazio tridimensionale e
 * una classe Segmentoper rappresentare un segmento nello spazio tridimensionale 
 * Scrivere una classe di test che crei: 
 * �due oggetti della classe Punto con coordinate (1, 3, 8) e (4, 4, 7) 
 * �un oggetto della classe Segmento che rappresenti il segmento che unisce i due punti di cui sopra
 * 
 * */
public class Punto {

	
	private int x;	//ascissa
	private int y;	//ordinata
	private int z;	//profondita
	
	public Punto(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	//i getter servono ad accedere a delle propriet� dell'oggetto
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}
}
