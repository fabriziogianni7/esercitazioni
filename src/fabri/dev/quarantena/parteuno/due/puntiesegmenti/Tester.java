package fabri.dev.quarantena.parteuno.due.puntiesegmenti;
/*
 * Progettare una classe Puntoper la rappresentazione di un punto nello spazio tridimensionale e
 * una classe Segmentoper rappresentare un segmento nello spazio tridimensionale 
 * Scrivere una classe di test che crei: 
 * �due oggetti della classe Punto con coordinate (1, 3, 8) e (4, 4, 7) 
 * �un oggetto della classe Segmento che rappresenti il segmento che unisce i due punti di cui sopra
 * ADDIZIONALE: CALCOLA LA LUNGHEZZA DEL SEGMENTO NELLO SPAZIO 3D
 * 
 * */
public class Tester {

	public static void main(String[] args) {
		Punto p1 = new Punto(1, 3, 8);
		Punto p2 = new Punto(4, 4, 7);
		Segmento seg = new Segmento(p1, p2);
		
		//formula per la lughezza del segmento nello spazio= rad(x1+x2)^2+(y1+y2)^2+(z1+z2)^2
		
		double lunghezzaSeg =
				//utilizzo la libreria Math e i suoi metodi per fare la formula.
				//utilizzo i getter dell'oggetto punto per prendere i valori delle coordinate
				Math.sqrt(Math.pow((p1.getX()+p2.getX()), 2)
						 +Math.pow((p1.getY()+p2.getY()), 2)
						 +Math.pow((p1.getZ()+p2.getZ()), 2));
		System.out.print("la lunghezza del segmento nello spazio 3d �: "+lunghezzaSeg);
	}

}
