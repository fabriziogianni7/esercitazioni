package fabri.dev.quarantena.parteuno.due.stringamagica;
/*	Progettare una classe Stringa42,
 *  costruita a partire da 3 stringhe in input, che concateni le stringhe inframezzate da spazi 
 *  e conservi solo i primi 42 caratteri della stringa risultante --> concatenaStringhe
 *   
 *   �La classe deve poter: 
 *   	�restituire la stringa di lunghezza massima 42
 *    	�restituire l�iniziale di tale stringa
 *    	
 *    Esempio: la Stringa �magica�
 *   	�restituire l�iniziale di tale stringa
 *    	�restituire un booleano che indichi se la stringa � pari al numero �magico� 42 
 *    
 *    nell'esempio delle slide la risoluzione � differente ma questo svolgimento da 
 *    pressoch� lo stesso risultato
*/
public class Stringa42 {
	private String input1;
	private String input2;
	private String input3;
	
	public Stringa42(String input1, String input2, String input3) {
		this.input1 = input1;
		this.input2 = input2;
		this.input3 = input3;
	}
	 
	public boolean isMagicaOrContiene() {
		boolean itIs = false;
		if(concatenaStringhe().equals("42") || concatenaStringhe().indexOf("42") != -1)
			itIs = true;
		return itIs; 	
	}
	
	public char restituisciIniziale() {
		char iniziale = concatenaStringhe().charAt(0);
		return iniziale;
	}
	
	public String restituisciStringa42() {
		String st = concatenaStringhe();
		if (concatenaStringhe().length() > 42) 
			st.substring(0,42);
		return st;
	}
	
	public String concatenaStringhe() {
		String spazio = " ";
		String as = input1.concat(spazio).concat(input2).concat(spazio).concat(input3);
		return as;
	}
}
