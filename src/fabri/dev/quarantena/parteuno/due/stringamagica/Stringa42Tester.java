package fabri.dev.quarantena.parteuno.due.stringamagica;

public class Stringa42Tester {

	public static void main(String[] args) {
		Stringa42 s42 = new Stringa42("ciao", "suca", "belli");
		
		System.out.println(s42.restituisciStringa42());
		System.out.println(s42.isMagicaOrContiene());
		System.out.println(s42.restituisciIniziale());
	}

}
