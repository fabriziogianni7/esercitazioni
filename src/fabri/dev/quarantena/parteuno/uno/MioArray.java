package fabri.dev.quarantena.parteuno.uno;
/* pag32
 * Progettare una classe MioArray i cui oggetti vengono costruiti con un array di interi (int[ ]),
 * La classe implementa i seguenti metodi: 
 	–contiene, che data in ingresso una posizione e un intero, restituisce true o false
 	se l’intero è contenuto in quella posizione nell’array 
 	
 	–somma2, che restituisce la somma dei primi due elementi dell’array. 
 	Se l’array è di lunghezza inferiore (info: la lunghezza dell’array a si ottiene con il campo speciale length, quindi a.length),
 	restituisce il valore del primo elemento oppure 0 se l’array è vuoto 
   
	–scambia, che date in ingresso due posizioni intere, scambia i valori presenti nelle due posizioni dell’array 
	(es. scambia(1, 3) trasforma l’array { 1, 2, 3, 4, 5 } in { 1, 4, 3, 2, 5 }) 
	
	–maxTripla: che restituisce il valore massimo tra il primo, l’ultimo e il valore in posizione intermedia dell’array 
	(es. restituisce 3 se l’oggetto è costruito con { *1, 7, 5, *3, 0, 2, *2}, le posizione esaminate hanno l'asterisco) 
	
	–falloInDue: che restituisce un array di due interi, il primo è il primo elemento dell’array dell’oggetto,
 	il secondo è l’ultimo elemento dell’array dell’oggetto
 * 
 */
public class MioArray {
	int[] arrayInteri;
	
	public MioArray(int[] arrayInteri) {
		this.arrayInteri = arrayInteri;
	}
	public int[] falloInDue () {
		int[] arrayDueValori = new int[0];
		arrayDueValori[0] = arrayInteri[0];
		arrayDueValori[1]  = arrayInteri[arrayInteri.length-1];
		return arrayDueValori;
		
	}
	public int maxTripla() {
		int primoVal = arrayInteri[0];
		int ultimoVal = arrayInteri[arrayInteri.length-1];
		int valIntermedio = arrayInteri[(arrayInteri.length-1)/2];
		int valMax = Math.max(primoVal,Math.max(ultimoVal, valIntermedio));
		
		return valMax;
	}
	public void scambia(int pos1, int pos2) {
		/*considerare che le posizioni si contano a partire da zero, la lunghezza dell'array, se contiene
		 l'elemento 0, è 1*/
		if(arrayInteri.length > pos1+1 && arrayInteri.length > pos2+1) {
			int variabileAusiliaria = arrayInteri[pos1];
			arrayInteri[pos1] = arrayInteri[pos2];
			arrayInteri[pos2] = variabileAusiliaria;
		}
	}
	
	public int somma2() {
		int somma = 0;
		if(arrayInteri.length > 2)
			somma = arrayInteri[0] + arrayInteri[1];
		return somma;
	}
	
	public boolean contiene(int posizione, int numero) {
		//significa: contenuto è true  se la il numero dell'array nella posizione data è uguale a quello inserito, altrimenti è false
		boolean contenuto = arrayInteri[posizione] == numero ?  true : false;
		
//		la stessa cosa di qui sotto
//		boolean contenuto = false;
//		if(arrayInteri[posizione] == numero)
//			contenuto = true;
		return contenuto;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
