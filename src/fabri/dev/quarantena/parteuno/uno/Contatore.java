package fabri.dev.quarantena.parteuno.uno;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Contatore {
	/*
	 * Progettare una classe che modelli un contatore intero
		con le seguenti operazioni:
		– Incrementa il contatore
		– Restituisci il valore attuale del contatore
		– Resetta il contatore a un valore fornito in input
	 * */
	
	private static  int i;
	
	public Contatore() {
		i=0;
	}
	private  int getVal(int i) {	return i;}

	private  int aggiungi() { return getVal(i++); }
	
	private  int reset() { return getVal(i=0);}
	
	private String chiedi() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("aggiungere[add], resettare[res] o chiudere[close]?");
		String s = br.readLine();
		return s;
	}
	
	public static void stampa(int i) { System.out.println(i);}
	public static void main(String[] args) throws IOException {
		Contatore cn = new Contatore();
		
		for(int n= 0; n<100; n++) {
			String s= cn.chiedi();
			if(s.contentEquals("add"))
				stampa(cn.aggiungi());
			else if(s.contentEquals("res"))
				stampa(cn.reset());
			else if(s.contentEquals("close"))
				break;
		}
			
		
	}

}
