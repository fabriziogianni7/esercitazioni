package fabri.dev.quarantena.parteuno.uno.rettangolo;
/*
 * Progettare una classe Rettangolo i cui oggetti
	rappresentano un rettangolo e sono costruiti a partire
	dalle coordinate x, y e dalla lunghezza e altezza del
	rettangolo
	
	La classe implementa i seguenti metodi:
	â€“ trasla che, dati in input due valori x e y, trasla le coordinate del
	rettangolo dei valori orizzontali e verticali corrispondenti
	â€“ toString che restituisce una stringa del tipo â€œ(x1, y1)->(x2, y2)â€�
	con i punti degli angoli in alto a sinistra(x=0) e in basso a destra(y=0) del
	rettangolo
	
	 Implementare una classe di test TestRettangolo che
	verifichi il funzionamento della classe Rettangolo sul
	rettangolo in posizione (0, 0) e di lunghezza 20 e
	altezza 10, traslandolo di 10 verso destra e 5 in basso
 */
public class Rettangolo {
	private long x;
	private long y;
	private long altezza;
	private long base;
	
	
	//attenzione, questo è il costruttore. significa che i parametri che gli passo in questo metodo 
	//rappresentano dei valori di input passati a questa classe.
	//this è una variabile di referenza che si riferisce a qualcosa di questo oggetto -> in questo caso 
	//alle variabili private istanziate precedentemente.
	public Rettangolo(long x, long y, long altezza, long base) {
		this.x = x;
		this.y = y;
		this.altezza = altezza;
		this.base = base;
	}
	
	public void trasla(long x2, long y2) {
		x = x +x2;
		y = y+y2;
	}
	
	public String inStringa() {
		//punto in alto a sx: x=0 --> x non si muove
		long x1 = x;
		long y1 = y+altezza;
		
		//punto in basso a destra y=0 y non si muove
		long x2 = x1+base;
		long y2 = y;
		return "("+x1+","+y1+")->("+x2+","+y2+")";
	}
}

