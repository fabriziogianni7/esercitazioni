package fabri.dev.quarantena.parteuno.uno.rettangolo;
/* Implementare una classe di test TestRettangolo che
	verifichi il funzionamento della classe Rettangolo sul
	rettangolo in posizione (0, 0) e di lunghezza 20 e
	altezza 10, traslandolo di 10 verso destra e 5 in basso
*/
public class RettangoloTest {
	public static void main(String[] args) {
		Rettangolo rett = new Rettangolo(0, 0, 10, 20);
		rett.trasla(10, -5);
		System.out.println(rett.inStringa());
		
	}
}
