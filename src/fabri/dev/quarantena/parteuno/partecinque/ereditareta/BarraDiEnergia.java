package fabri.dev.quarantena.parteuno.partecinque.ereditareta;

/*Creare una classe BarraDiEnergiacostruita con un intero k che ne determina la lunghezza massima.
 *  Inizialmente la barra � vuota. 
 *  La barra � dotata di un metodo per l�incremento unitario del suo livello di riempimento 
 *  e di un metodo toStringche ne fornisca la rappresentazione sotto forma di stringa 
 *  (es. se il livello � 3 su 10, la stringa sar� �OOO=======�. 
 *  
 *  
 *  Creare quindi una seconda classe BarraDiEnergiaConPercentuale che 
 *  fornisce una rappresentazione sotto forma di stringa come BarraDiEnergia 
 *  ma stampando in coda alla stringa la percentuale del livello di riempimento. 
 *  Per esempio, se il livello � 3 su 10, la stringa sar� �OOO======= 30%�.
*/

public abstract class BarraDiEnergia {

	protected double k; //lunghezza max
	protected double l; //livello energia
	
	public BarraDiEnergia(double k, double l) {
		this.k = k;
		this.l = l;
	}
	
	public double incrementa() {
		return this.l+1;
	}
	
	public String toStringa() {
		String carica= "";
		for(int i=1; i<=k; i++) {
			if(i<=this.l)
				carica+="0";
			else
				carica+= "=";
		}
		return carica;
	}
	
}
