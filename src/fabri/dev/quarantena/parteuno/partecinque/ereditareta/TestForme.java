package fabri.dev.quarantena.parteuno.partecinque.ereditareta;

public class TestForme {

	public static void main(String[] args) {
		Quadrato q = new Quadrato("rosso", 4);
		//metodo della superclasse
		System.out.println("colore= "+q.getColore());
		//metodo della sottoclasse
		System.out.println("perimetro= "+q.calcolaPerimetro());

	}

}
