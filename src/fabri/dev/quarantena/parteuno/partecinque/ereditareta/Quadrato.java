package fabri.dev.quarantena.parteuno.partecinque.ereditareta;

public class Quadrato extends Forma{
	private int lato;
	/*la parola chiave super richiama qualcosa della superclasse
	*in questo caso, messa obbligatoriamente sulla prima linea del costruttore
	*permette di chiamare il costruttore della superclasse
	*/
	
	//esempi di oveloading
	public Quadrato(String colore, int lato) {
		super(colore);
		this.lato = lato;
	}
	
	//il this usato alla prima linea del costruttore si riferisce al costruttore di questa classe
	public Quadrato(String colore) {
		this(colore,1);
	}

	
	//esempio di overriding
	@Override
	protected int calcolaPerimetro() {
		int perimetro = lato*4;
		return perimetro;
	}


}
