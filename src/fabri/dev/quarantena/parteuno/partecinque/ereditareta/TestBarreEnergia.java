package fabri.dev.quarantena.parteuno.partecinque.ereditareta;

public class TestBarreEnergia {

	public static void main(String[] args) {
		BarraDiEnergiaConPercentuale b = new BarraDiEnergiaConPercentuale(10, 3);
		b.incrementa();
		//metodo della superclasse
		System.out.println("metodo della superclasse: "+b.toStringa());
		//metodo della sottoclasse
		System.out.println("metodo della sottoclasse: "+b.toStringa()+" "+b.calcolaPercent());

	}

}
