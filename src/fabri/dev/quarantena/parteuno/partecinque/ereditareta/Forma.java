package fabri.dev.quarantena.parteuno.partecinque.ereditareta;

//superclasse Forma - definisce un comportamento comune
 abstract public class Forma {

	private String colore;
	
	public Forma(String colore) {
		this.colore = colore;
	}
	
	protected String getColore() {
		return colore;
	}
	
	protected abstract int calcolaPerimetro();
}
