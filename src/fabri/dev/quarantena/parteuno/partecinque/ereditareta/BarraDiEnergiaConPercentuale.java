package fabri.dev.quarantena.parteuno.partecinque.ereditareta;

public class BarraDiEnergiaConPercentuale extends BarraDiEnergia{

	public BarraDiEnergiaConPercentuale(int k, int l) {
		super(k, l);
	}
	
	public String calcolaPercent() {
		double perc = l/k*100;
		String p = perc+"%";
		return p;
	}

}
