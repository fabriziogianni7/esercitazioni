package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.giocodelloca;

public class Giocatore {
	
	private String nome;
	private int posizione;
	private int punti;
	
	//all'inizio ogni giocatore avrà 0 pt e partirà dalla casella 0
	public Giocatore(String nome) {
		this.nome = nome;
		posizione =0;
		setPunti(0);
	}

	//restituisce random da 1 a 6 e aggiunge alla posizione
	public int tiraDado() {
		 int risultato = (int)((Math.random())*6+1);
		 return risultato;
	}
	
	public String getNome() {
		return nome;
	}
	
	public int getPosizione() {
		return posizione;
	}
	public void setPosizione(int posizione) {
		this.posizione=posizione;
	}

	public int getPunti() {
		return punti;
	}

	public void setPunti(int punti) {
		this.punti = punti;
	}
	

}
