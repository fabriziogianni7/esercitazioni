package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.giocodelloca;

public class CasellaPunti extends Casella{

	public CasellaPunti(int posizione) {
		super(posizione);
	}

	@Override
	public String stampa() {
		String stampaPunti="_$_ ";
		if(!super.stampa().equals(""))
			return super.stampa()+stampaPunti;
		return stampaPunti;
	}

	@Override
	public Giocatore effetto(Giocatore giocatore) {
		giocatore.setPunti(giocatore.getPunti()+3);
		return giocatore;
	}

}
