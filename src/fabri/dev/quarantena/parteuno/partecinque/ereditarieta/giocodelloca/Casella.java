package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.giocodelloca;

public abstract class Casella {

	protected int posizione;
	protected Giocatore[] giocatoriSuCasella;
	
	
	public Giocatore[] getGiocatoriSuCasella() {
		return giocatoriSuCasella;
	}
	public int getPosizione() {
		return posizione;
	}

	public void setGiocatoriSuCasella(Giocatore[] giocatoriSuCasella) {
		this.giocatoriSuCasella = giocatoriSuCasella ;
	}

	public Casella(int posizione) {
		this.posizione =posizione;
	}
	
	public String stampa() {
		String stampa = "";
		if(giocatoriSuCasella != null) {
			for(Giocatore g : giocatoriSuCasella) {
				if(g!= null){
					stampa +=g.getNome();
				}
			}
		}
		return stampa;
	}
	public abstract Giocatore effetto(Giocatore giocatore);
}
