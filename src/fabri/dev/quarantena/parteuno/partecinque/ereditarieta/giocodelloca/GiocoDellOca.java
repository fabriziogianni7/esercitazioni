package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.giocodelloca;

import java.util.Scanner;

public class GiocoDellOca {
	public static void main(String[] args) {
		System.out.print("Benvenuto su GiocoDellOcaDiFabrizio, clicca un tasto qualunque per iniziare: ");
		Scanner sc = new Scanner(System.in);
		String avvia = sc.nextLine();
		if(!avvia.equalsIgnoreCase("")) {
			Tabellone t = new Tabellone(4);
			stampaTabellone(t);
			giocaTurno(t);
		}
	}
	
	private static void giocaTurno(Tabellone t) {
		Giocatore[] giocatori = t.getElencoGiocatori();
		for(Giocatore g : giocatori) {
			Scanner sc = new Scanner(System.in);
			System.out.print(g.getNome()+", clicca 'g' ed effettua la tua mossa, altrimenti 'e' per uscire: ");
			String decisione = sc.nextLine();
			
			//mossa del giocatore
			if(decisione.equalsIgnoreCase("g")) {
				int dado = g.tiraDado();
				g.setPosizione(g.getPosizione()+dado);
				
				//riposizionamento
				Casella[] caselle = t.getListaCaselle();
				for(Casella c : caselle) {
					//effetto
					if(g.getPosizione() == c.getPosizione())
						c.effetto(g);
					t.posizionaGiocatori();
				}
			}
			if (decisione.equalsIgnoreCase("e")) {
				System.out.println("Si � scelto di terminare il gioco. Addio!");
				stampaTabellone(t);
				stampaClassifica(t);
				break;
				
			}
			stampaTabellone(t);
			stampaClassifica(t);
			
		}
	}
	
	private static void stampaTabellone(Tabellone t) {
		for(int i = 0; i<=t.getListaCaselle().length-1;i++) {
			System.out.print(t.getListaCaselle()[i].stampa());
		}
		System.out.println();
		System.out.println();
	}
	
	private static void stampaClassifica(Tabellone t) {
		for(int i = 0; i<=t.getElencoGiocatori().length-1;i++) {
			Giocatore g = t.getElencoGiocatori()[i];
			System.out.println(g.getNome()+": posizione: "+g.getPosizione()+"; punti: "+g.getPunti());
		}
		System.out.println();
		System.out.println();
	}
	

	
		
	

}
