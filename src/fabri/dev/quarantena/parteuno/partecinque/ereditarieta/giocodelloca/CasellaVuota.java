package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.giocodelloca;

public class CasellaVuota extends Casella{

	public CasellaVuota(int posizione) {
		super(posizione);
	}

	@Override
	public String stampa() {
		String stampaVuota="_ ";
		if(!super.stampa().equals(""))
			return super.stampa()+stampaVuota;
		return stampaVuota;
	}
	

	@Override
	public Giocatore effetto(Giocatore giocatore) {
		return giocatore;
	}

}
