package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.Tetris;

public abstract class Pezzo {

	protected String forma;
	protected int puntiPezzo;
	
	public String getForma() {
		return forma;
	}
	public void setForma(String forma) {
		this.forma = forma;
	}
	public int getPuntiPezzo() {
		return puntiPezzo;
	}
	public void setPuntiPezzo(int puntiPezzo) {
		this.puntiPezzo = puntiPezzo;
	}
	
	
	
}
