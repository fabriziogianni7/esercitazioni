package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.animali;

public class Millepiedi extends Animale{

	@Override
	protected String emettiVerso() {
		verso="millepiedi: taptaptap";
		return verso;
	}
	@Override
	protected long getNumeroZampe() {
		nZampe = 1000;
		return nZampe;
	}
	
	@Override
	protected String getTaglia() {
		taglia = "piccola";
		return taglia;
	}
}
