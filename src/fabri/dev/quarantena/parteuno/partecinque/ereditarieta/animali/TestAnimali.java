package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.animali;
/**
Progettare la classe Animale che rappresenti un generico animale
* •La classe possiede i metodi emettiVerso()e getNumeroDiZampe() 
* •Possiede inoltre il metodo getTaglia() che restituisce un valore scelto tra: piccola, media e grande. 
* 
* •Progettare quindi le classi 
* 	Mammifero, 
* 			Felino, 
* 				Gatto(taglia piccola), Tigre(grande), 
* 			Cane, 
* 				Chihuahua(piccola), Beagle (media), Terranova (grande), 
* 			Uccello,
* 				 Corvo(media), Passero (piccola), 
*  
*  Millepiedi (piccola) 
*  
*  •Personalizzare in modo appropriato la taglia, il numero di zampe e il verso degli animali*/

public class TestAnimali {

	public static void main(String[] args) {
		//cani
		Beagle beagle = new Beagle();
		System.out.println(beagle.emettiVerso());
		System.out.println(beagle.getNumeroZampe());
		System.out.println(beagle.getTaglia());
		System.out.println();
		Chiuaua chiuaua = new Chiuaua();
		System.out.println(chiuaua.emettiVerso());
		System.out.println(chiuaua.getNumeroZampe());
		System.out.println(chiuaua.getTaglia());
		System.out.println();
		
		Terranova terranova = new Terranova();
		System.out.println(terranova.emettiVerso());
		System.out.println(terranova.getNumeroZampe());
		System.out.println(terranova.getTaglia());
		System.out.println();

		//felini
		Gatto gatto = new Gatto();
		System.out.println(gatto.emettiVerso());
		System.out.println(gatto.getNumeroZampe());
		System.out.println(gatto.getTaglia());
		gatto.graffia();
		System.out.println();

		Tigre tigre = new Tigre();
		System.out.println(tigre.emettiVerso());
		System.out.println(tigre.getNumeroZampe());
		System.out.println(tigre.getTaglia());
		tigre.graffia();
		System.out.println();
		
		//uccelli
		Corvo corvo = new Corvo();
		System.out.println(corvo.emettiVerso());
		System.out.println(corvo.getNumeroZampe());
		System.out.println(corvo.getTaglia());
		System.out.println();
		
		Passero passero = new Passero();
		System.out.println(passero.emettiVerso());
		System.out.println(passero.getNumeroZampe());
		System.out.println(passero.getTaglia());
		System.out.println();
		
		Millepiedi millepiedi = new Millepiedi();
		System.out.println(millepiedi.emettiVerso());
		System.out.println(millepiedi.getNumeroZampe());
		System.out.println(millepiedi.getTaglia());
		System.out.println();
		
		
		
		
		
	}

}
