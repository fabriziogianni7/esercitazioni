package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.animali;
/**
Progettare la classe Animale che rappresenti un generico animale
* •La classe possiede i metodi emettiVerso()e getNumeroDiZampe() 
* •Possiede inoltre il metodo getTaglia() che restituisce un valore scelto tra: piccola, media e grande. 
* 
* •Progettare quindi le classi 
* 	Mammifero, 
* 			Felino, 
* 				Gatto(taglia piccola), Tigre(grande), 
* 			Cane, 
* 				Chihuahua(piccola), Beagle (media), Terranova (grande), 
*  Uccello, 
*  		Corvo(media), Passero (piccola), 
*  
*  Millepiedi (piccola) 
*  
*  •Personalizzare in modo appropriato la taglia, il numero di zampe e il verso degli animali*/

public class Tigre extends Felino{

	@Override
	protected void graffia() {
		System.out.println("squarcio della tigre");
	}
	
	@Override
		protected String emettiVerso() {
			verso = "rooarr";
			return verso;
		}
	
	@Override
	protected String getTaglia() {
		taglia = "tigre: taglia grande";
		return taglia;
	}
}
