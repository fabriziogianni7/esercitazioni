package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.contoBancario;
/*
 * Progettare la classe ContoBancario che rappresenti un conto con 
 * informazioni relative al denaro attualmente disponibile,
 *  il codice IBAN 
 *  
 * •Modellare quindi una generica operazione bancaria Operazioneche disponga di un metodo esegui() 
 * 
 * •Modellare quindi i seguenti tipi di operazione:
 * –PrelevaDenaro: preleva una specificata quantità di denaro da un dato conto 
 * –SvuotaConto: preleva tutto il denaro da un dato conto 
 * –VersaDenaro: versa del denaro sul conto specificato 
 * –SituazioneConto: stampa l’attuale saldo del conto 
 * –Bonifico: preleva del denaro da un conto e lo versa su un altro
 * 
 * 
 * 
 * */
public class ContoBancario {

	private String iban;
	private long saldo;
	
	public ContoBancario(String iban, long saldo) {
		this.iban = iban;
		this.saldo = saldo;
	}
	
	
	public long getSaldo() {
		return saldo;
	}
	public long setSaldo(long saldo) {
		this.saldo = saldo;
		return saldo;
	}

	public String getIban() {
		return iban;
	}

	
}
