package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.contoBancario;
/*
 * Progettare la classe ContoBancario che rappresenti un conto con 
 * informazioni relative al denaro attualmente disponibile,
 *  il codice IBAN 
 *  
 * •Modellare quindi una generica operazione bancaria Operazioneche disponga di un metodo esegui() 
 * 
 * •Modellare quindi i seguenti tipi di operazione:
 * –PrelevaDenaro: preleva una specificata quantità di denaro da un dato conto 
 * –SvuotaConto: preleva tutto il denaro da un dato conto 
 * –VersaDenaro: versa del denaro sul conto specificato 
 * –SituazioneConto: stampa l’attuale saldo del conto 
 * –Bonifico: preleva del denaro da un conto e lo versa su un altro
 * 
 * 
 * 
 * */
public class TestBonifico {

	public static void main(String[] args) {
		ContoBancario contoDiFabrizio = new ContoBancario("contofabri123", 100);
		//inizializzo tutte le operazioni
		SituazioneConto situazioneConto = new SituazioneConto();
		PrelevaDenaro preleva = new PrelevaDenaro(10);
		SvuotaConto svuota = new SvuotaConto();
		VersaDenaro versaDenaro = new VersaDenaro(20);
		ContoBancario contoDestinatario = new ContoBancario("contodestinatario123", 20);
		Bonifico bonifico = new Bonifico(contoDestinatario, 30);
		
		//situazione conto iniziale
		System.out.println("situazione iniziale $: "+situazioneConto.esegui(contoDiFabrizio));
		System.out.println();
		//preleva
		System.out.println("preleva: "+preleva.esegui(contoDiFabrizio));
		System.out.println("situazione dopo prelievo (90) $: "+situazioneConto.esegui(contoDiFabrizio));
		System.out.println();
		//versa
		System.out.println("versa: "+versaDenaro.esegui(contoDiFabrizio));
		System.out.println("situazione dopo versameto (110) $: "+situazioneConto.esegui(contoDiFabrizio));
		System.out.println();
		//bonifico
		System.out.println("bonifico: "+bonifico.esegui(contoDiFabrizio));
		System.out.println("situazione dopo bonifico (80) $: "+situazioneConto.esegui(contoDiFabrizio));
		System.out.println("situazione conto destinatatrio (80) $: "+situazioneConto.esegui(contoDestinatario));
		System.out.println();
		//svuota
		System.out.println("svuotaconto: "+svuota.esegui(contoDiFabrizio));
		System.out.println("situazione dopo conto svuotato (0) $: "+situazioneConto.esegui(contoDiFabrizio));
		System.out.println();
		
		
	}
}
