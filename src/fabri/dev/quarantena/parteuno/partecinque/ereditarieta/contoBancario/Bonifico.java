package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.contoBancario;
/*
 * Progettare la classe ContoBancario che rappresenti un conto con 
 * informazioni relative al denaro attualmente disponibile,
 *  il codice IBAN 
 *  
 * •Modellare quindi una generica operazione bancaria Operazioneche disponga di un metodo esegui() 
 * 
 * •Modellare quindi i seguenti tipi di operazione:
 * –PrelevaDenaro: preleva una specificata quantità di denaro da un dato conto 
 * –SvuotaConto: preleva tutto il denaro da un dato conto 
 * –VersaDenaro: versa del denaro sul conto specificato 
 * –SituazioneConto: stampa l’attuale saldo del conto 
 * –Bonifico: preleva del denaro da un conto e lo versa su un altro
 * 
 * 
 * 
 * */
public class Bonifico extends OperazioneBancaria{

	private ContoBancario contoDestinazione;
	private long importo;
	
	public Bonifico(ContoBancario contoDestinazione, long importo) {
		this.contoDestinazione = contoDestinazione;
		this.importo = importo;
	}
	@Override
	protected long esegui(ContoBancario conto) {
		//sottrai l'importo dal conto 
		conto.setSaldo(conto.getSaldo()-importo);
		long saldoDestinazione = contoDestinazione.setSaldo(contoDestinazione.getSaldo()+importo);
		return saldoDestinazione;
	}

}
