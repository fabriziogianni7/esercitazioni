package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.distributore;
/*
 * 
 * •Progettare una classe Prodotto con un prezzo e tre tipi diversi di prodotto: 
 *  Caffè, Cappuccino, Cioccolato 
 * 
 * •Progettare la classe DistributoreDiBevande che rappresenti un distributore automatico costruito con un intero N che determina 
 *  il numero di prodotti nel distributore
 *   
 * •La classe prevede i seguenti metodi: 
 * –un metodo carica() che inserisce N prodotti di tipo e ordine casuale 
 * –un metodo inserisciImporto() che permette di inserire un importo nella macchinetta 
 * –un metodo getProdotto() che, dato in ingresso un numero di prodotto, restituisca il prodotto associato 
 * 	a quel numero e decrementi il saldo disponibile nel distributore 
 * –Un metodo getSaldo() che restituisca il saldo attuale del distributore 
 * –un metodo getResto() che restituisca il resto dovuto e azzeri il saldo
 * 
 * */
public class DistributoreBevande {
	
	private int nProdotti;
	private double saldo;
	private Prodotto[] prodotti;
	
	public DistributoreBevande(int nProdotti) {
		this.nProdotti=nProdotti;
		this.saldo=5;
		this.prodotti = new Prodotto[nProdotti];
		carica();
		System.out.println("il saldo all'accensione del distributore è: "+saldo);
	}
	
	public void carica() {
		for(int i=0; i<=nProdotti-1;i++) {
			double random = Math.random();
			if(random<=0.3) {
				prodotti[i] = new Caffe();
			}
			if(random >0.3 && random <=0.6) {
				prodotti[i] =new Cappuccino();
			}
			if(random > 0.6) {
				prodotti[i] =new Cioccolato();
			}
		}
		
		
		System.out.println("prodotti attualmente nel distributore:" );
		for(int i=0; i<=prodotti.length-1;i++) {
			System.out.print("prodotto n."+i+": "+prodotti[i].getNomeProdotto());
			System.out.println("; prezzo: "+prodotti[i].getPrezzoProdotto());
		}
	}

	public double inserisciImporto(double importo) {
		 saldo = saldo+importo;
		System.out.println();
		System.out.println("importo inserito: "+importo);
		System.out.println("saldo attuale: "+saldo);
		return saldo+importo;
	}
	
	//compra prodotto
	public void getProdotto(int numeroProdotto) {
		Prodotto prod = prodotti[numeroProdotto];
		System.out.println();
		System.out.print("il prodotto scelto: "+prod.getNomeProdotto());
		System.out.println(" "+prod.getPrezzoProdotto()+"$");
		
		double nuovoSaldo = saldo-prod.getPrezzoProdotto();
		saldo = nuovoSaldo;
		System.out.println("nuovo saldo: "+saldo+"$");
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	//da il resto
	public double getResto() {
		double resto = saldo;
		saldo = 0;
		System.out.println();
		System.out.println("ecco il resto: "+resto);
		return resto;
	}
}
