package fabri.dev.quarantena.parteuno.partecinque.ereditarieta.distributore;
/*
 * 
 * •Progettare una classe Prodotto con un prezzo e tre tipi diversi di prodotto: 
 *  Caffè, Cappuccino, Cioccolato 
 * 
 * •Progettare la classe DistributoreDiBevande che rappresenti un distributore automatico costruito con un intero N che determina 
 *  il numero di prodotti nel distributore
 *   
 * •La classe prevede i seguenti metodi: 
 * –un metodo carica() che inserisce N prodotti di tipo e ordine casuale 
 * –un metodo inserisciImporto() che permette di inserire un importo nella macchinetta 
 * –un metodo getProdotto() che, dato in ingresso un numero di prodotto, restituisca il prodotto associato 
 * 	a quel numero e decrementi il saldo disponibile nel distributore 
 * –Un metodo getSaldo() che restituisca il saldo attuale del distributore 
 * –un metodo getResto() che restituisca il resto dovuto e azzeri il saldo
 * 
 * */
public class TestDistributore {

	public static void main(String[] args) {
		DistributoreBevande distributore = new DistributoreBevande(10);
		distributore.inserisciImporto(1);
		distributore.getProdotto(5);
		distributore.getSaldo();
		distributore.getResto();

	}

}
