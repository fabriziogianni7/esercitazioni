package fabri.dev.quarantena.intro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 *Scrivere una classe Variabili che, all’interno del metodo main, 
 *dichiari una variabile intera i, una variabile j
 *di tipo stringa s e una variabile double d. 
 *Quindi vengono svolte le seguenti tre operazioni: 
 *–La stringa viene inizializzata al valore del primo argomento fornito in input al main 
 *–All’intero viene assegnato il valore intero della stringa 
 *–All’tero viene assegnato il valore intero della stringa 
 *–Al double viene assegnata la metà del valore di i (ad es. se i è pari a 3, d sarà pari a 1.5) 
 *–I valori di s, i e d vengono stampati a video  
 * 
 * */
public class Variabili {

	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Inserire valore");
		String s = br.readLine();
		int i = Integer.parseInt(s);
		double d = i/2;
		
		System.out.println("S= "+s+" Integer= "+i+" Double= "+d);
		
		
	}
}
