package fabri.dev.quarantena.intro;

import java.util.Random;

/*Progettare una classe i cui oggetti contengono tre elenchi di parole l1, l2e l3
 * La classe è in grado di emettere nuove espressioni costruite creando stringhe del tipo “a b c”
 *  scegliendo casualmente dai tre rispettivi elenchi a ϵl1, b ϵl2, c ϵl3
 *  Ad esempio, dati i seguenti elenchi: 
 
	–l1= { “salve”, “ciao”, “hello”, “buongiorno”, “scialla” }
 	–l2= { “egregio”, “eclettico”, “intelligentissimo”, “astutissimo” }
 	–l3= { “studente”, “ragazzo”, “giovane”, “scapestrato”, “fannullone”, “studioso” }  
 	
 	Esempi di outputsono: –“salve egregio fannullone” –“ciao eclettico scapestrato” */
public class GeneraFrasi {
	
	String[] saluti = {"ciao ", "salve ","benvenuto "};
	String[] animali = {" scimmio ", " cane "," maiale "," gatto "," pipistrello "," orca assassina "};
	String[] insulti = {" stupido", " coglione"," sciocco"," stolto"};
	
	public int generaNumero(int max) { 
		Random rn = new Random();//Math.Random() avrebbe sempre dato zero
		return (rn.nextInt(max));
	}
	
	public String generaFrase() {
		return (saluti[generaNumero(saluti.length)]+animali[generaNumero(animali.length)]+
				insulti[generaNumero(insulti.length)]);
	}
	
	public static void main (String[] args) {
		GeneraFrasi gen = new GeneraFrasi();
		System.out.println(gen.generaFrase());
	}

}
